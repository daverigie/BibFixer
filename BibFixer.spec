# -*- mode: python -*-
a = Analysis(['bin/BibFixer.py'],
             pathex=['C:\\Users\\daver_000\\Dropbox\\BibFixer'],
             hiddenimports=[],
             hookspath=None,
             runtime_hooks=None)
pyz = PYZ(a.pure)
exe = EXE(pyz,
          a.scripts,
          a.binaries,
          a.zipfiles,
          a.datas,
          name='BibFixer.exe',
          debug=False,
          strip=None,
          upx=True,
          console=False )
