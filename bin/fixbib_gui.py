#!python
from Tkinter import *
import tkFileDialog
import fixbib as fixbib
import ttk
import tkMessageBox
import time

    
def main():
    
    def callback():
        p = fpath.get()
        g = fixbib.parse_bib(p)
    
        start = time.time()
    
        for (n_bad, n_good, N) in g:
            pbar.step(1.0*100/N)
            pbar.update()
            
            elapsed = time.time()-start
            avg_time = elapsed/(n_bad + n_good)
            
            s1 = "{0:.0f} seconds remaining\n\n".format(avg_time*(N-n_good-n_bad))
            s2 = "{0} of {1} entries processed\n".format(n_good+n_bad, N) 
            s3 = "{0} entries found in CrossRef\n".format(n_good)
            
            l.set(s1 + s2 + s3)
        
        s1 = "Done!\n\n"
        l.set(s1 + s2 + s3)	

    def helpMsg():
        msg = """\
              This program can be used to fix .bib files by looking each entry up on CrossRef by its DOI. Metadata found in CrossRef will be merged into the existing entries. If the DOI cannot be successfully queried, the entry will be left as is. Extra tags in your bib entries that are not in CrossRef will also be left alone. A new file will be formed in input directory named "input-file-name_fixed.bib"
              """
        msg.replace("\n", "")
        msg = msg.strip()
        msg.replace("\t", "")
        tkMessageBox.showinfo("About", msg)
        
        
    def filebrowser():
        filetypes = [('BibTex Files', '.bib'), ('All Files', '.*')]
        fpath.set(tkFileDialog.askopenfilename(filetypes=filetypes))

    class PathEntry(Entry):

        def __init__(self, prompt, *args, **kwargs):
            self.prompt = prompt
            Entry.__init__(self, *args, **kwargs)
            self.insert(0, prompt)
            self.bind('<Button-1>', self.getFocus)
            self.bind("<FocusOut>")
            
        def getFocus(self, event):
            self.delete(0, 'end')
            
        def loseFocus(self, event):
            self.insert(0, self.prompt)
    
    master = Tk()
    master.wm_title("BibTex Fixer")

    padding = dict(padx = 5.0, pady = 5.0)

    fpath = StringVar()
    entry_path = PathEntry("Path to .bib file", master, width=50, textvariable=fpath, justify=CENTER)
    entry_path.grid(row=0,column=1,padx=5.0)

        
    button_open = Button(master, text="Open", width=10, command=filebrowser)
    button_open.grid(row=0,column=0,padx=5.0)
        
    button_run = Button(master, text="Run", width=10, command=callback)
    button_run.grid(row=1,column=0,padx=5.0)

    pbar = ttk.Progressbar(master, length=300)
    pbar.grid(row=1, column=1, padx=5.0)

    l = StringVar()
    label_progress = Label(master, text = "", textvariable=l, justify=LEFT)
    label_progress.grid(row=3, column=1)

    button_help = Button(master, text="?", command=helpMsg)
    button_help.grid(row=4, column = 1, sticky=S+E)
    mainloop()

if __name__ == "__main__":
    main()





