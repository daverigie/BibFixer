#!python

#import requests
import re
import sys
import argparse
import urllib2
import time
import os
from datetime import datetime

def stamped_path(path):
      
      stamped_path = path + "." 
        
      now = datetime.now()
      
      stamp = (str(now.year), str(now.month), str(now.day), 
               str(now.hour), str(now.minute), str(now.second))
               
      stamp = str.join('-',stamp)
      
      stamped_path += stamp
      
      return stamped_path
      
def progress_bar(iterable, N, bar_full = 20):
    	
    counter = 0
    
    start = time.time()
    avg_time = 1.0
    
    for elem in iterable:
		counter += 1
		bar_part = int((counter/float(N))*bar_full)
                             
		elapsed = time.time()-start
		avg_time_last = avg_time*1.0
		avg_time = elapsed/counter
		time_remaining = (N-counter)*avg_time
    
		sys.stdout.write("\r")
		sys.stdout.write("[" + "-"*bar_part + " "*(bar_full-bar_part) + "] " + \
                         "{0} of {1} ... ".format(counter, N))
       
          
		sys.stdout.write("{0:.0f} seconds remaining".format(time_remaining))
        
		sys.stdout.flush()
        
                             
		yield elem

def lowerKeys(d):
    
    d2 = {}
    
    for key, val in d.iteritems():
        d2[key.lower().strip()] = val.strip()
        
    return d2

class BibEntry:
    
    def __init__(self, txt):
        
        if txt is None:
            raise ValueError('BAD DOI')  
        
        entryType, citationKey = re.findall("@([^{]*){([^,]*)", txt)[0]
        
        
        attributes = dict(re.findall("(.*)\s*=\s*{(.*)}", txt))
        attributes = lowerKeys(attributes)
        
        self.entryType = entryType
        self.citationKey = citationKey
        self.attributes = attributes
        
    @property
    def txt(self):
        
        s = "@" + self.entryType + "{" + self.citationKey + ",\n"
        
        keys = sorted(self.attributes.keys())
        if "note" in keys:
            keys.remove("note")
            keys.insert(0, "note")
        for key in keys:
            val = self.attributes[key]
            s += key + " = {" + val + "},\n"
            
        s += "}"
        
        return s                  
                        
def getEntries(fulltext):

    entries = []
    
    # break text up into unique bibtex entries
    entries_txt = re.findall("@[^{]*{[^@]*}", fulltext, flags = re.DOTALL)    
    for txt in entries_txt:      
        entries.append(BibEntry(txt))
        
    return entries

def mergeEntries(entry, entry_cross):
        
    if entry_cross is None:
        raise ValueError("Bad DOI")
        
    entry.entryType = entry_cross.entryType
    entry.citationKey = entry_cross.citationKey
    entry.attributes.update(entry_cross.attributes)
    
    return entry
    
def parse_bib(bibpath_in, bibpath_out = None):
    
    # read bibtex file
    f = open(bibpath_in, "r")
    fulltext = f.read()
    f.close()
    
    # scan through bibtex entries and try to extract DOI
    entries = getEntries(fulltext)
    
    if bibpath_out == None:
		name, extension = re.findall('([^\.]*)(\..*)',bibpath_in)[0]
		bibpath_out = name + "_fixed" + extension
			
    f = open(bibpath_out, "w+")
    
    N = len(entries)

    n_good = 0
    n_bad = 0
    
    for n,entry_cross in enumerate(progress_bar(crossRefs(entries), N)):   
        try: 
            mergeEntries(entries[n], entry_cross)
            entries[n].attributes['note'] = "Updated from CrossRef using BibFixer"
            n_good += 1
        except:
            entries[n].attributes['note'] = "Unable to update from CrossRef, bad DOI?"
            n_bad += 1
			
        yield n_bad, n_good, N
        f.write("\n\n" + entries[n].txt)    
        
    f.close()

    

def crossRef(entry):
    
    doi = entry.attributes['doi']
    entry_cross = BibEntry(doi2bib(doi))
    
    return entry_cross
    
def crossRefs(entries):

    for entry in entries:
        try:
            yield crossRef(entry)
        except:
            yield None
def doi2bib(doi):
    url = "http://dx.doi.org/" + doi
    opener = urllib2.build_opener()
    opener.addheaders = [('Accept', 'application/x-bibtex')]
    try:
        r = opener.open(url)
        if r.msg != "OK":
            raise ValueError("BAD DOI")
        return r.read()
    except:
        return None
        
def dois2bib(dois):
    
    opener = urllib2.build_opener()
    opener.addheaders = [('Accept', 'application/x-bibtex')]
    for doi in dois:
        try:
            url = "http://dx.doi.org/" + doi
            r = opener.open(url)
            if r.msg != "OK":
                raise ValueError("BAD DOI")
            yield r.read()
        except:
            yield None
        
def main(args):
 
	for (n_bad, n_good, N) in parse_bib(args.input, args.output):
		pass

	print("\n\nFixed {0} of {1} bibtex entries using CrossRef".format((n_good), N))
	
if __name__ == "__main__":
	main()