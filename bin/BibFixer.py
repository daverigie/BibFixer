#!python
import sys

if len(sys.argv) == 1:
    
    import fixbib_gui
    fixbib_gui.main()
    
else:
    import argparse
    import fixbib
    
    msg = """\
              This program can be used to fix .bib files by looking each entry up on CrossRef by its DOI. Metadata found in CrossRef will be merged into the existing entries. If the DOI cannot be successfully queried, the entry will be left as is. Extra tags in your bib entries that are not in CrossRef will also be left alone. A new file will be formed in input directory named "input-file-name_fixed.bib"
              """
    epilog = "\n\nExample:\n\n./BibFixer.py inputfile.bib -o outputfile.bib"
    
    parser = argparse.ArgumentParser(description = msg, epilog = epilog)
    parser.add_argument("input")
    parser.add_argument("--output", "-o", default = None)
    args = parser.parse_args()
    fixbib.main(args)
    